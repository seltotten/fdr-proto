module gitlab.com/selftotten/fdr-proto

go 1.13

require (
	github.com/gogo/protobuf v1.3.1
	github.com/metaverse/truss v0.1.0
	google.golang.org/grpc v1.26.0
)
